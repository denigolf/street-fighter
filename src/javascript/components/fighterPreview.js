import { createElement } from '../helpers/domHelper';
import '../../styles/additional.css';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const { source, name, health, attack, defense } = fighter;

    const fighterImg = createElement({
      tagName: 'img',
      className: `fighter-preview__img`,
      attributes: {
        src: source,
      },
    });

    const fighterContentContainer = createElement({
      tagName: 'div',
      className: `fighter-preview__content-container`,
    });

    const fighterName = createElement({
      tagName: 'p',
    });

    const fighterHealth = createElement({
      tagName: 'p',
    });

    const fighterAttack = createElement({
      tagName: 'p',
    });

    const fighterDefense = createElement({
      tagName: 'p',
    });

    fighterName.innerText = `Name: ${name}`;
    fighterHealth.innerText = `Health: ${health}`;
    fighterAttack.innerText = `Attack: ${attack}`;
    fighterDefense.innerText = `Defense: ${defense}`;

    fighterContentContainer.append(fighterImg, fighterName, fighterHealth, fighterAttack, fighterDefense);
    fighterElement.append(fighterContentContainer);

    return fighterElement;
  } else {
    const fighterContentContainer = createElement({
      tagName: 'div',
      className: `fighter-preview__content-container`,
    });

    const message = createElement({
      tagName: 'p',
      className: `fighter-preview__content-container__message`,
    });

    message.innerText = 'Choose another hero!';
    message.style.textAlign = 'center';
    message.style.borderBottom = 'none';

    fighterContentContainer.append(message);
    fighterElement.append(fighterContentContainer);

    return fighterElement;
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
