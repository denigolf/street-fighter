import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    let keysPressed = [];

    firstFighter.totalHealth = firstFighter.health;
    secondFighter.totalHealth = secondFighter.health;
    firstFighter.lastCriticalHit = 0;
    secondFighter.lastCriticalHit = 0;

    document.addEventListener('keydown', (event) => {
      const code = event.code;
      keysPressed.push(code);

      startAttack(firstFighter, secondFighter, keysPressed, firstFighter.totalHealth, secondFighter.totalHealth);

      if (secondFighter.health <= 0) {
        resolve(firstFighter);
      }

      if (firstFighter.health <= 0) {
        resolve(secondFighter);
      }
    });

    // Remove released keys
    document.addEventListener('keyup', (event) => {
      const code = event.code;
      keysPressed = keysPressed.filter((key) => key !== code);
    });
  });
}

export function startAttack(attacker, defender, keysPressed) {
  const leftIndicator = document.querySelector('#left-fighter-indicator');
  const rightIndicator = document.querySelector('#right-fighter-indicator');

  const isAttackerAttack = keysPressed.includes(controls.PlayerOneAttack);
  const isAttackerCriticalAttack = controls.PlayerOneCriticalHitCombination.every((item) => keysPressed.includes(item));
  const isDefenderAttack = keysPressed.includes(controls.PlayerTwoAttack);
  const isDefenderCriticalAttack = controls.PlayerTwoCriticalHitCombination.every((item) => keysPressed.includes(item));

  switch (true) {
    case isAttackerAttack:
      dealHit(attacker, defender, rightIndicator, keysPressed);
      break;
    case isAttackerCriticalAttack:
      dealCriticalHit(attacker, defender, rightIndicator);
      break;
    case isDefenderAttack:
      dealHit(defender, attacker, leftIndicator, keysPressed);
      break;
    case isDefenderCriticalAttack:
      dealCriticalHit(defender, attacker, leftIndicator);
      break;
  }
}

// Indicator (health bar)
export function updateIndicator(indicator, defender) {
  const currentHealthPercent = (defender.health / defender.totalHealth) * 100;
  const indicatorCurrentWidth = currentHealthPercent >= 0 ? currentHealthPercent : 0;
  indicator.style.width = indicatorCurrentWidth + '%';
}

// Attack
export function dealHit(attacker, defender, indicator, keysPressed) {
  if (!isBlock(keysPressed)) {
    const damage = getDamage(attacker, defender);
    defender.health -= damage;
    updateIndicator(indicator, defender, defender.totalHealth);
  }
}

// Critical Attack
export function dealCriticalHit(attacker, defender, indicator) {
  if (getCriticalHitStatus(attacker)) {
    const damage = attacker.attack * 2;
    defender.health -= damage;
    updateIndicator(indicator, defender, defender.totalHealth);
    attacker.lastCriticalHit = new Date();
  }
}

// Block
export function isBlock(keysPressed) {
  const isAttackerBlock = keysPressed.includes(controls.PlayerOneBlock);
  const isDefenderBlock = keysPressed.includes(controls.PlayerTwoBlock);

  return isAttackerBlock || isDefenderBlock ? true : false;
}

export function getCriticalHitStatus(fighter) {
  const currentTime = new Date();
  const secondsPassed = Math.abs((currentTime - fighter.lastCriticalHit) / 1000);
  const isCriticalHitAvailable = secondsPassed >= 10 ? true : false;
  return isCriticalHitAvailable;
}

// Damage functions
export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  const realDamage = damage <= 0 ? 0 : damage;
  return realDamage;
  // return damage
}

export function getHitPower(fighter) {
  const criticalDamage = getRandomWithinRange(2, 1);
  const attack = fighter.attack;
  const hitPower = attack * criticalDamage;
  return hitPower;
  // return hit power
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandomWithinRange(2, 1);
  const defense = fighter.defense;
  const blockPower = defense * dodgeChance;
  return blockPower;
  // return block power
}

// Helper functions
export function getRandomWithinRange(min, max) {
  return Math.random() * (max - min) + min;
}
