import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function
  const { name, source } = fighter;

  const fighterImg = createElement({
    tagName: 'img',
    className: ``,
    attributes: {
      src: source,
    },
  });

  showModal({ title: `${name} wins!`, bodyElement: fighterImg });
}
